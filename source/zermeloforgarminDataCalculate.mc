using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;


class zermeloforgarminDataCalculate {
    function initialize() as Void {}
    function weekdayfromdate(date){
        var SECONDS_PER_WEEK = Gregorian.SECONDS_PER_DAY * 7;
        var lastmonday = zermeloforgarminDataCalculate.lastmonday(date);
        var options = {
            :year => Gregorian.info(Time.now(),Time.FORMAT_SHORT).year,
            :month => 1,
            :day => 1
        };
        var firstdayoftheyear = Gregorian.moment(options);
        var firstmondayoftheyear = Gregorian.info(firstdayoftheyear,Time.FORMAT_SHORT).day_of_week;
        if (firstmondayoftheyear == 1){
            firstmondayoftheyear = 8;
        }
        firstmondayoftheyear = -1 * (firstmondayoftheyear - 9);
        firstmondayoftheyear = firstdayoftheyear.add(new Time.Duration(Gregorian.SECONDS_PER_DAY * firstmondayoftheyear));

        var delta = lastmonday.value() - firstmondayoftheyear.value();
        return delta / SECONDS_PER_WEEK +1;
    }
    function lastmonday(date){
        var calcdate = Gregorian.info(date,Time.FORMAT_SHORT);
        var SECONDS_PER_WEEK = Gregorian.SECONDS_PER_DAY * 7;
        var dayssindsmonday = calcdate.day_of_week;
        if (dayssindsmonday == 1){
            dayssindsmonday = 8;
        }
        dayssindsmonday -= 2;
        return date.add(new Time.Duration(-Gregorian.SECONDS_PER_DAY * dayssindsmonday));
    }
    function unixtodate(unixtime){
        var options = {
            :year => 1970,
            :month => 1,
            :day => 1
        };
        var unix = Gregorian.moment(options);
        return unix.add(new Time.Duration(unixtime));
    }
}