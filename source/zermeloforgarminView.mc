import Toybox.Graphics;
import Toybox.WatchUi;
using Toybox.Time;
using Toybox.Time.Gregorian;

class zermeloforgarminView extends WatchUi.View {
  hidden var daytxt;
  public var log = false;
  function initialize() {
    View.initialize();
  }

  // Load your resources here
  function onLayout(dc as Dc) as Void {
    setLayout(Rez.Layouts.MainLayout(dc));
  }

  // Called when this View is brought to the foreground. Restore
  // the state of this View and prepare it to be shown. This includes
  // loading resources into memory.
  function onShow() as Void {}

  // Update the view
  function onUpdate(dc as Dc) as Void {
    if (log){
      System.println("update");
    }
    View.findDrawableById("date").setText(zermeloforgarminDataFormat.datetostring(date));
    if (log){
      System.println(zermeloforgarminDataFormat.datetostorestring(date));
    }
    var i = 0;
    var available = false;
    while (i<7){
      var daytotest;
      if (i > 0){
        var days = new Time.Duration(Gregorian.SECONDS_PER_DAY * i);
        daytotest = zermeloforgarminDataCalculate.lastmonday(date).add(days);
      } else {
        daytotest = zermeloforgarminDataCalculate.lastmonday(date);
      }
      var result = false;
      result = (zml.schedule[zermeloforgarminDataFormat.datetostorestring(daytotest)] != null);
      available = available || result;
      i++;
    }
    if (log){
      System.println("available: "+available);
    }
    if (!available && zml.failed == null && zml.loggingin){
      if (zml.loggedin()){
        zml.getSchedule(Gregorian.info(date,Time.FORMAT_LONG).year,zermeloforgarminDataCalculate.weekdayfromdate(date));
      } else {
        WatchUi.requestUpdate();
      }
    }
    var today = null;
    today = zml.schedule[zermeloforgarminDataFormat.datetostorestring(date)];
    System.print("date:");
    System.println(zermeloforgarminDataFormat.datetostorestring(date));
    if (zml.failed != null){
      View.findDrawableById("available").setText(zml.failed);
      View.findDrawableById("schedule").setText("");
    } else if (!available){
      View.findDrawableById("available").setText("loading schedule...");
    } else if (today == null){
      View.findDrawableById("available").setText("no schedule available today");
      View.findDrawableById("schedule").setText("");
    } else {
      View.findDrawableById("available").setText("schedule available");
      View.findDrawableById("schedule").setText(zermeloforgarminDataFormat.scheduletostring(today));
    }
    View.onUpdate(dc);
  }

  // Called when this View is removed from the screen. Save the
  // state of this View here. This includes freeing resources from
  // memory.
  function onHide() as Void {}
}
