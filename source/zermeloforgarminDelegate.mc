import Toybox.Lang;
import Toybox.WatchUi;
using Toybox.Math;
using Toybox.Time;
using Toybox.Time.Gregorian;

class zermeloforgarminDelegate extends WatchUi.BehaviorDelegate {

    function initialize() {
        BehaviorDelegate.initialize();
    }

    function onMenu() as Boolean {
        WatchUi.pushView(new Rez.Menus.main(), new zermeloforgarminMenuMain(), WatchUi.SLIDE_UP);
        return true;
    }
    function onPreviousPage() as Boolean {
        System.println("up");
        date = date.add(new Time.Duration(Gregorian.SECONDS_PER_DAY));
        WatchUi.requestUpdate();
        return true;
    }
    function onNextPage() as Boolean {
        System.println("down");
        date = date.add(new Time.Duration(-Gregorian.SECONDS_PER_DAY));
        WatchUi.requestUpdate();
        return true;
    }
    function onBack() as Boolean{
        System.println("back");
        var pdate = date;
        date = zermeloforgarminDataGet.date();
        WatchUi.requestUpdate();
        return !(zermeloforgarminDataFormat.removetimefromdate(date).value() == zermeloforgarminDataFormat.removetimefromdate(pdate).value());
    }
}