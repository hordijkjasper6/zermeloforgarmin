using Toybox.Time;
using Toybox.Time.Gregorian;
using Toybox.System;
using Toybox.Communications;
class zermeloforgarminApi {
    private var token;
    private var accessToken;
    private var password;
    private var username;
    private var domain;
    private var userType;
    private var proxyurl;
    public var schedule;
    public var schedulestate;
    public var failed = null;
    public var loggingin = false;
    function initialize(tenant,accountType) as Void {
        domain = tenant;
        userType = accountType;
        token = null;
        accessToken = null;
        schedule = {};
        schedulestate = null;
    }
    public function login(usr,psw,proxy) {
        loggingin = true;
        username = usr;
        password = psw;
        token = null;
        accessToken = null;
        proxyurl = proxy;
        getToken();
    }
    public function loginv2(linkcode){
        loggingin = true;
        getAccessTokenv2(linkcode);
    }

    public function loggedin() as Boolean{
        return (accessToken != null);
    }
    public function scheduleavailable() as Boolean{
        return (schedule != null && schedule != :loading);
    }
    function onReceiveAccessTokenv2(responseCode, data){
        System.println(responseCode);
        if (responseCode > 199 && responseCode < 300){
            accessToken = data["access_token"];
            System.println(accessToken);
        } else if (responseCode == 	-104 || responseCode == -1004){
            failed = "phone disconnected";
        } else if (responseCode == 	-300){
            failed = "server unavaileble";
        } else if (responseCode == 	-403){
            failed = "out of memory";
        }else {
            failed = "failed to login in: " + responseCode.toString()+"\n(get token)";
        }
    }
    private function getAccessTokenv2(appcode) {
       var url = "https://"+domain+".zportal.nl/api/v3/oauth/token?grant_type=authorization_code&code="+appcode.toString(); //.toNumber().toString();

       var params = {};

       var options = {
           :method => Communications.HTTP_REQUEST_METHOD_POST,
        //    :headers => {"Content-Type" => Communications.REQUEST_CONTENT_TYPE_URL_ENCODED},
           :responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON
       };
       var responseCallback = method(:onReceiveAccessTokenv2);
       Communications.makeWebRequest(url, params, options, responseCallback);
    }


    function onReceiveToken(responseCode, data){
        System.println(responseCode);
        System.println(data);
        if (responseCode > 199 && responseCode < 300){
            token = data["token"];
            System.println(token);
            getAccessToken();
        } else if (responseCode == 	-104 || responseCode == -1004){
            failed = "phone disconnected";
        } else if (responseCode == 	-300){
            failed = "server unavaileble";
        } else if (responseCode == 	-403){
            failed = "out of memory";
        }else {
            failed = "failed to login in: " + responseCode.toString()+"\n(get token)";
        }
    }
    private function getToken() {
    //    var url = "https://"+domain+".zportal.nl/api/v3/oauth?username="+username+"&password="+password+"&client_id=OAuthPage&redirect_uri=/main/&scope=&state=4E252A&response_type=code&tenant="+domain+"";
    //    var url = "https://btpv.pemvedr.nl/zermeloforgarminproxy";
        var url = proxyurl;

        var params = {"domain" => domain,"username" => username, "password" => password};
    //    var params = {};

        var options = {
                :method => Communications.HTTP_REQUEST_METHOD_POST,
                :headers => {"Content-Type" => Communications.REQUEST_CONTENT_TYPE_JSON},
                :responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON

        };
        var responseCallback = method(:onReceiveToken);
        Communications.makeWebRequest(url, params, options, responseCallback);
    }


    function onReceiveAccessToken(responseCode, data){
        System.println(responseCode);
        if (responseCode > 199 && responseCode < 300){
            accessToken = data["access_token"];
            System.println(accessToken);
        } else if (responseCode == 	-104 || responseCode == -1004){
            failed = "phone disconnected";
        } else if (responseCode == 	-300){
            failed = "server unavaileble";
        } else if (responseCode == 	-403){
            failed = "out of memory";
        }else {
            failed = "failed to login in: " + responseCode.toString()+" get access token";
        }
    }

    private function getAccessToken() {
       var url = "https://"+domain+".zportal.nl/api/v3/oauth/token?code="+token+"&client_id=ZermeloPortal&client_secret=42&grant_type=authorization_code&rememberMe=false";

       var params = {};

       var options = {
           :method => Communications.HTTP_REQUEST_METHOD_POST,
        //    :headers => {"Content-Type" => Communications.REQUEST_CONTENT_TYPE_URL_ENCODED},
           :responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON
       };
       var responseCallback = method(:onReceiveAccessToken);
       Communications.makeWebRequest(url, params, options, responseCallback);
    }
    function onReceiveSchedule(responseCode, data){
        System.println(responseCode);
        if (responseCode > 199 && responseCode < 300){
            var rawschedule = data["response"]["data"][0]["appointments"];
            var wi = -1;
            var week = [0,0,0,0,0,0,0];
            var options = {
                :year => 1970,
                :month => 1,
                :day => 1
            };
            var unix = Gregorian.moment(options);
            var ld = unix;
            for( var i = 0; i < rawschedule.size(); i++ ) {
                var date = zermeloforgarminDataFormat.removetimefromdate(unix.add(new Time.Duration(rawschedule[i]["start"])));
                if (date.value() != ld.value()){
                    ld = date;
                    wi++;
                }
                week[wi] += 1;
            }
            wi = -1;
            var di = 0;
            ld = unix;
            var day = null;
            for( var i = 0; i < rawschedule.size(); i++ ) {
                var date = zermeloforgarminDataFormat.removetimefromdate(unix.add(new Time.Duration(rawschedule[i]["start"])));
                if (date.value() != ld.value()){
                    if (day != null){
                        schedule[zermeloforgarminDataFormat.datetostorestring(ld)] = day;
                    }
                    wi++;
                    day = new [week[wi]];
                    ld = date;
                    di = 0;
                    // wi++;
                }
                day[di] = rawschedule[i];
                di++;
            }
            schedule[zermeloforgarminDataFormat.datetostorestring(ld)] = day;
            WatchUi.requestUpdate();
        } else if (responseCode == 	-104 || responseCode == -1004){
            failed = "phone disconnected";
        } else if (responseCode == 	-300){
            failed = "server unavaileble";
        } else if (responseCode == 	-403){
            failed = "out of memory";
        } else {
            failed = "failed to get schedule";
        }
    }

    public function getSchedule(year,week) {
        schedulestate = :loading;
        var url = "https://"+domain+".zportal.nl/api/v3/liveschedule?access_token="+accessToken+"&"+userType+"="+username+"&week="+year.toString()+week.toString();
        var params = {};
        var options = {
            :method => Communications.HTTP_REQUEST_METHOD_GET,
            // :headers => {"Content-Type" => Communications.REQUEST_CONTENT_TYPE_URL_ENCODED},
            :responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON
        };
        var responseCallback = method(:onReceiveSchedule);
        Communications.makeWebRequest(url, params, options, responseCallback);
    }
}