import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
using Toybox.Time;
using Toybox.Time.Gregorian;

var date;
var zml;
var app;
class zermeloforgarminApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
        // var test = new JsonTransaction();
        // test.makeRequest();
        app =  Application.getApp();
        date = zermeloforgarminDataGet.date();
        zml = new zermeloforgarminApi(app.getProperty("Domain"),app.getProperty("Student")? "student" : "teacher");
        if (app.getProperty("Username") == null || app.getProperty("Password")  == null || app.getProperty("Domain") == null){
            // zml.failed = "no login";
        }
        zml.login(app.getProperty("Username"), app.getProperty("Password"),app.getProperty("Proxy_Server"));
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new zermeloforgarminView(), new zermeloforgarminDelegate() ] as Array<Views or InputDelegates>;
    }

}

function getApp() as zermeloforgarminApp {
    return Application.getApp() as zermeloforgarminApp;
}