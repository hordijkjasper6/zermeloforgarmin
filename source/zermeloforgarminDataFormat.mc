using Toybox.System;
using Toybox.Time;
using Toybox.Time.Gregorian;


class zermeloforgarminDataFormat {
    function initialize() as Void {}
    function numberstodate(year as Lang.Number,month as Lang.Number,day as Lang.Number) as Void {
        var options = {
            :year   => year, 
            :month  => month,
            :day    => day,
        };
        return Gregorian.moment(options);
    }
    function datetostring(date) as Void{
        date = Gregorian.info(date,Time.FORMAT_LONG);
        return Lang.format(
        "$1$ $2$ $3$ $4$",
        [
            date.day_of_week,
            date.day,
            date.month,
            date.year
        ]
        );
    }
    function timetostring(time) as Void{
        var date = Gregorian.info(time,Time.FORMAT_LONG);
        var hour = date.hour;
        var min = date.min;
        if (min == 0){
            min = "00";
        }
        return Lang.format(
        "$1$:$2$",
        [
            hour,
            min
        ]
        );
    }
    function datetostorestring(date) as Void{
        date = Gregorian.info(date,Time.FORMAT_SHORT);
        return Lang.format(
        "$1$-$2$-$3$",
        [
            date.year,
            date.month,
            date.day
        ]
        );
    }
    function removetimefromdate(date) as Void{
        date = Gregorian.info(date,Time.FORMAT_SHORT);
        var options = {
            :year   => date.year, 
            :month  => date.month,
            :day    => date.day,
        };
        return Gregorian.moment(options);
    }
    function scheduletostring(schedule){
        var lessons = "";
        // var student = false;
        var student = app.getProperty("Student");
        for( var i = 0; i < schedule.size(); i++ ) {
            if (schedule[i]["status"][0]["code"] < 4000){
                var les = schedule[i];
                if (student? les["subjects"].toString(): les["groups"].toString().substring(1, -1).length() <= 1) {
                    lessons += ((les["subjects"].size() > 0)? les["subjects"][0] : "")+"("+((les["locations"].size() > 0)? les["locations"][0] : "")+")    "+zermeloforgarminDataFormat.timetostring(zermeloforgarminDataCalculate.unixtodate(les["start"]))+"-"+zermeloforgarminDataFormat.timetostring(zermeloforgarminDataCalculate.unixtodate(les["end"]))+"\n";
                } else {
                    lessons += student? les["subjects"].toString(): les["groups"].toString().substring(1, -1)+"("+les["locations"].toString().substring(1, -1)+")    "+zermeloforgarminDataFormat.timetostring(zermeloforgarminDataCalculate.unixtodate(les["start"]))+"-"+zermeloforgarminDataFormat.timetostring(zermeloforgarminDataCalculate.unixtodate(les["end"]))+"\n";
                }
            }
        }
        return lessons;
    }
}